import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders} from '@angular/common/http';
import { Empresa } from '../model/empresa';
import { Observable } from "rxjs";
import { Cep } from  "../model/Cep"

@Injectable({
  providedIn: 'root'
})
export class CepService {

  constructor(private http: HttpClient) {  }
  
    findByCep(cep){
      return this.http.get<Cep>(`https://viacep.com.br/ws/${cep}/json/`);
    };
}
