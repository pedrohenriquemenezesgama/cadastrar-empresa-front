import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders} from '@angular/common/http';
import { Empresa } from '../model/empresa'
import {COMPANY_API} from './company.api';
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class EmpresasService {

   httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};

  constructor(private http: HttpClient) { }

  findAll(pageSize:number,pageNo:number): Observable<Empresa[]>{
   return this.http.get<Empresa[]>(`${COMPANY_API}/empresas?pageSize=${pageSize}&pageNo=${pageNo}`);
 }

 save(empresa:Empresa){
   return this.http.post(`${COMPANY_API}/empresas`,empresa,this.httpOptions);
 };

findOne(id){
  return this.http.get<Empresa>(`${COMPANY_API}/empresas/buscar/${id}`);
};

delete(id){
  return this.http.delete(`${COMPANY_API}/empresas/${id}`,{ responseType: 'text' })
}

findEmpresaByCnpj(cnpj){
  return this.http.get<Empresa[]>(`${COMPANY_API}/empresas/buscar/cnpj?value=${cnpj}`);
}

findEmpresaByNome(nome){
  return this.http.get<Empresa[]>(`${COMPANY_API}/empresas/buscar/nome?value=${nome}`);
}

findEmpresaByCnpjAndNome(cnpj,nome){
  console.log(`${COMPANY_API}/empresas/buscar/nome/cnpj?nome=${nome}&cnpj=${nome}`);
  return this.http.get<Empresa[]>(`${COMPANY_API}/empresas/buscar/nome/cnpj?nome=${nome}&cnpj=${nome}`);
}

}
// http://localhost:8080/empresas?pageSize=5&pageNo=1
