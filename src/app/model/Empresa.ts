export interface  Empresa {
  id: number;
  nome:string;
  cnpj:string;
  contato:string;
  cep:string;
  bairro:string;
  logradouro:string;
  tipoEmpresa:string;
  razaoSocial:string;
  email:string;
  estado:string;
  cidade:string;
  complemento:string
}
