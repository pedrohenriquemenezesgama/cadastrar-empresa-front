import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmpresaComponent } from './components/empresa/empresa.component';
import { EmpresasService} from './services/empresas.service';
import { HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FormsModule,ReactiveFormsModule  }   from '@angular/forms';
import { CadastroEmpresaComponent } from './components/cadastro-empresa/cadastro-empresa.component';
import { EditarEmpresaComponent } from './components/editar-empresa/editar-empresa.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';



@NgModule({
  declarations: [
    AppComponent,
    EmpresaComponent,
    CadastroEmpresaComponent,
    EditarEmpresaComponent,
    NavBarComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,NgbModule,FormsModule,ReactiveFormsModule
  ],
  providers: [HttpClientModule,EmpresasService],
  bootstrap: [AppComponent]
})
export class AppModule { }
