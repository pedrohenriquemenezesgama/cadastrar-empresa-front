import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmpresaComponent } from './components/empresa/empresa.component';
import { CadastroEmpresaComponent } from './components/cadastro-empresa/cadastro-empresa.component';
import { EditarEmpresaComponent } from './components/editar-empresa/editar-empresa.component';

const routes: Routes = [
  { path: '', redirectTo: '/empresas', pathMatch: 'full' },
  { path: 'empresas', component: EmpresaComponent },
  { path: 'cadastro', component: CadastroEmpresaComponent },
  { path: 'editar/:id', component: EditarEmpresaComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
