import { Component, OnInit } from '@angular/core';
import { EmpresasService } from '../../services/empresas.service';
import { Empresa } from '../../model/Empresa'
import { Observable } from "rxjs";
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.css']
})
export class EmpresaComponent implements OnInit {

  pageSize:number=5;
  pageNo:number=0;
  listEmpresa = [];
  previousPage: any;
  message = ''

  empresaForm = new FormGroup({
  nome: new FormControl(''),
  cnpj: new FormControl('')
 });



  constructor(private empresaService: EmpresasService) { }


  findAll(pageSize:number,pageNo:number){
  this.empresaService.findAll(pageSize,pageNo).subscribe(res => {
          this.listEmpresa = res;
        });
 }

  findEmpresa() {

    if(this.empresaForm.value.nome.trim().length === 0 && this.empresaForm.value.cnpj.trim().length > 0 ){
      this.empresaService.findEmpresaByCnpj(this.empresaForm.value.cnpj).subscribe(res =>{
        if(res.length > 0){
          this.listEmpresa = res;
            this.message = ""
        }else{
          this.message = "Empresa não encontrada"
        }
           })
    }else if(this.empresaForm.value.cnpj.trim().length === 0 && this.empresaForm.value.nome.trim().length > 0){
      this.empresaService.findEmpresaByNome(this.empresaForm.value.nome).subscribe(res =>{
        if(res.length > 0){
          this.listEmpresa = res;
          this.message = ""
        }else{
          this.message = "Empresa não encontrada"
        }
           })
    }else if(this.empresaForm.value.cnpj.trim().length > 0 && this.empresaForm.value.nome.trim().length > 0){
      this.empresaService.findEmpresaByCnpjAndNome(this.empresaForm.value.cnpj,this.empresaForm.value.nome).subscribe(res =>{
        if(res.length > 0){
          this.listEmpresa = res;
          this.message = ""
        }else{
          this.message = "Empresa não encontrada"
        }
           })
    }
     //  if (this.empresaForm.value.nome.trim() === 0) {
     //      this.empresaService.findEmpresaByCnpj(this.empresaForm.value.cnpj).subscribe(res =>{
     //        console.log(res)
     //      })
     //  } else if (this.empresaForm.value.cnpj.trim() === 0){
     //    this.empresaService.findEmpresaByCnpj(this.empresaForm.value.cnpj).subscribe(res =>{
     //      console.log(res)
     //    })
     // } else {
     //  this.empresaService.findEmpresaByCnpjAndNome(this.empresaForm.value.cnpj, this.empresaForm.value.cnpj).subscribe(res =>{
     //    console.log(res)
     //  })
  }

loadPage(pageNo) {
   if (pageNo !== this.previousPage) {
     this.previousPage = pageNo;
     this.loadData();
   }
 }

 loadData() {
   this.findAll(this.pageSize,this.pageNo -1 )
  }

  delete(id):void{
    this.empresaService.delete(id).subscribe(res =>{
      console.log(this.pageSize,this.pageNo)
    this.findAll(5,0)
    })
  }

  ngOnInit(): void {
    this.findAll(this.pageSize,this.pageNo)
  }

}
