import { Component, OnInit } from '@angular/core';
import { EmpresasService } from '../../services/empresas.service';
import { Empresa } from '../../model/Empresa'
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import {CepService} from '../../services/cep.service';

@Component({
  selector: 'app-cadastro-empresa',
  templateUrl: './cadastro-empresa.component.html',
  styleUrls: ['./cadastro-empresa.component.css']
})
export class CadastroEmpresaComponent implements OnInit {

  empresaForm = new FormGroup({
  nome: new FormControl('',Validators.required),
  cnpj: new FormControl('',Validators.required),
  contato: new FormControl('',Validators.required),
  cep: new FormControl('',Validators.required),
  bairro: new FormControl('',Validators.required),
  logradouro: new FormControl('',Validators.required),
  tipoEmpresa: new FormControl('',Validators.required),
  razaoSocial: new FormControl('',Validators.required),
  email: new FormControl('',[Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
  estado: new FormControl('',Validators.required),
  cidade: new FormControl('',Validators.required),
  complemento: new FormControl(''),
 });

 messageSucess = '';
 messageError = '';

  constructor(private empresaService: EmpresasService,private location: Location, private cepService: CepService) { }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    this.empresaService.save(this.empresaForm.value).subscribe(()=>{
      this.messageSucess = 'Empresa salva com sucesso!';
      this.messageError = '';
    }, err =>{
      this.messageSucess = '';
      this.messageError = 'Não foi possivel salvar empresa';
    });
    this.empresaForm.reset();

  }

  goBack(): void {
    this.location.back();
  }

  onKey(event: any) { // without type info
 let cep = event.target.value.replace(/\D/g, '');
 if(cep != ""){
 let validacep = /^[0-9]{8}$/;
  if(validacep.test(cep)) {
   this.cepService.findByCep(cep).subscribe(res => {
           this.empresaForm.patchValue({
           cep: res.cep,
           bairro:res.bairro,
           complemento: res.complemento,
           cidade: res.localidade,
           estado: res.uf
        
  });
         });
  }

 }

 }

  ngOnInit(): void {
  }

}
