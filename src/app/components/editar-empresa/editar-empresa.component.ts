import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { EmpresasService } from '../../services/empresas.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Empresa } from '../../model/Empresa';
import { Observable } from "rxjs";

@Component({
  selector: 'app-editar-empresa',
  templateUrl: './editar-empresa.component.html',
  styleUrls: ['./editar-empresa.component.css']
})
export class EditarEmpresaComponent implements OnInit {
  empresa:Empresa;

  messageSucess = '';
  messageError = '';

  empresaForm = new FormGroup({
  nome: new FormControl(''),
  cnpj: new FormControl(''),
  contato: new FormControl(''),
  cep: new FormControl(''),
  bairro: new FormControl(''),
  logradouro: new FormControl(''),
  tipoEmpresa: new FormControl(''),
  razaoSocial: new FormControl(''),
  email: new FormControl(''),
  estado: new FormControl(''),
  cidade: new FormControl(''),
  complemento: new FormControl(''),
 });

  constructor(private route: ActivatedRoute,private location: Location,private empresaService: EmpresasService) { }

  ngOnInit(): void {
    this.getEmpresa();
  }
  onSubmit() {
    // TODO: Use EventEmitter with form value
    this.empresaService.save(this.empresaForm.value).subscribe(()=>{
      this.messageSucess = 'Empresa salva com sucesso!';
    }, err =>{
      this.messageError = 'Não foi possivel salvar empresa';
    });
  }

  getEmpresa(): void{
     const id = +this.route.snapshot.paramMap.get('id');
     this.empresaService.findOne(id).subscribe((data: Empresa) => this.empresa = { ...data });
  }

  goBack(): void {
    this.location.back();
  }

}
